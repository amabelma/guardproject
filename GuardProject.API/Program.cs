﻿using System;
using GuardProject.Common;

namespace GuardProject.API
{
    class Program
    {
        static void Main(string[] args)
        {
            // This is just a simple example, so we'll pretend external parameters are provided
            Console.Write("Retrieving input parameters... ");
            string firstName = "Xiang";
            string lastName = "Na";
            long userId = 1356;
            string transactionInstructions = "<instructions><deposit>1500</deposit></instructions>";
            Console.WriteLine("Done.");

            var TransactionProcessor = new TransactionProcessor();

            Console.Write("Validating user... ");
            var userIsValid = TransactionProcessor.ValidateUser(firstName, lastName, userId);
            Console.WriteLine("Done.");

            Console.Write("Processing transaction... ");
            TransactionProcessor.ProcessTransaction(userId, transactionInstructions);
            Console.WriteLine("Done.");
        }
    }
}
