namespace GuardProject.Common
{
    public class TransactionProcessor
    {
        public TransactionProcessor()
        {
            this._dateTimeHelper = new DateTimeHelper();
        }
        public TransactionProcessor(IDateTimeHelper dateTimeHelper)
        {
            this._dateTimeHelper = dateTimeHelper;
        }
        public bool ValidateUser(string firstName, string lastName, long userId)
        {
            bool result = false;

            Guard.AgainstNullEmptyOrWhitespaceString(firstName, nameof(firstName));
            Guard.AgainstNullEmptyOrWhitespaceString(lastName, nameof(lastName));
            Guard.AgainstInvalidId(userId, nameof(userId));

            if (userId > 140 && userId < 67986)
            {
                result = true;
            }

            return result;
        }

        public void ProcessTransaction(long userId, string transactionInstructions)
        {
            Guard.AgainstInvalidId(userId, nameof(userId));
            Guard.AgainstInvalidXmlString(transactionInstructions, nameof(transactionInstructions));

            // Pretend like we need DateTime.Now for logging purposes\
            // var timestamp = DateTime.Now <-- for low energy losers
            var timestamp = _dateTimeHelper.CurrentTimeStamp();

            //blah blah do something cool
        }

        private IDateTimeHelper _dateTimeHelper = new DateTimeHelper();
    }
}