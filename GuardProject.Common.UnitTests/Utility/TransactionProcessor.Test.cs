using GuardProject.Common;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TestProject.Common.UnitTests
{
    [TestClass]
    public class TransactionProcessorTest
    {
        [TestClass]
        public class ValidateUserTest
        {
            [TestMethod]
            public void ValidateUser_GivenNullFirstname_ThrowsArgumentException()
            {
                var processor = new TransactionProcessor();
                string firstName = null;
                string lastName = "Test";
                long userId = 1234;

                Assert.ThrowsException<ArgumentException>(() => processor.ValidateUser(firstName, lastName, userId));
            }

            [TestMethod]
            public void ValidateUser_GivenEmptyFirstname_ThrowsArgumentException()
            {
                var processor = new TransactionProcessor();
                string firstName = "";
                string lastName = "Test";
                long userId = 1234;

                Assert.ThrowsException<ArgumentException>(() => processor.ValidateUser(firstName, lastName, userId));
            }

            [TestMethod]
            public void ValidateUser_GivenWhitespaceFirstname_ThrowsArgumentException()
            {
                var processor = new TransactionProcessor();
                string firstName = " ";
                string lastName = "Test";
                long userId = 1234;

                Assert.ThrowsException<ArgumentException>(() => processor.ValidateUser(firstName, lastName, userId));
            }

            [TestMethod]
            public void ValidateUser_GivenNullLastname_ThrowsArgumentException()
            {
                var processor = new TransactionProcessor();
                string firstName = "Test";
                string lastName = null;
                long userId = 1234;

                Assert.ThrowsException<ArgumentException>(() => processor.ValidateUser(firstName, lastName, userId));
            }

            [TestMethod]
            public void ValidateUser_GivenEmptyLastname_ThrowsArgumentException()
            {
                var processor = new TransactionProcessor();
                string firstName = "Test";
                string lastName = "";
                long userId = 1234;

                Assert.ThrowsException<ArgumentException>(() => processor.ValidateUser(firstName, lastName, userId));
            }

            [TestMethod]
            public void ValidateUser_GivenWhitespaceLastname_ThrowsArgumentException()
            {
                var processor = new TransactionProcessor();
                string firstName = "Test";
                string lastName = " ";
                long userId = 1234;

                Assert.ThrowsException<ArgumentException>(() => processor.ValidateUser(firstName, lastName, userId));
            }

            [TestMethod]
            public void ValidateUser_GivenNegativeUserid_ThrowsArgumentException()
            {
                var processor = new TransactionProcessor();
                string firstName = "Test";
                string lastName = "Test";
                long userId = -1234;

                Assert.ThrowsException<ArgumentException>(() => processor.ValidateUser(firstName, lastName, userId));
            }

            [TestMethod]
            public void ValidateUser_GivenZeroUserid_ThrowsArgumenetException()
            {
                var processor = new TransactionProcessor();
                string firstName = "Test";
                string lastName = "Test";
                long userId = 0;

                Assert.ThrowsException<ArgumentException>(() => processor.ValidateUser(firstName, lastName, userId));
            }

            [TestMethod]
            public void ValidateUser_GivenUseridLessThan140_ReturnsFalse()
            {
                var processor = new TransactionProcessor();
                string firstName = "Test";
                string lastName = "Test";
                long userId = 139;
                bool expected = false;

                bool actual = processor.ValidateUser(firstName, lastName, userId);

                Assert.AreEqual(expected, actual);
            }

            [TestMethod]
            public void ValidateUser_GivenUseridEquals140_ReturnsFalse()
            {
                var processor = new TransactionProcessor();
                string firstName = "Test";
                string lastName = "Test";
                long userId = 140;
                bool expected = false;

                bool actual = processor.ValidateUser(firstName, lastName, userId);

                Assert.AreEqual(expected, actual);
            }

            [TestMethod]
            public void ValidateUser_GivenUseridGreaterThan67986_ReturnsFalse()
            {
                var processor = new TransactionProcessor();
                string firstName = "Test";
                string lastName = "Test";
                long userId = 67987;
                bool expected = false;

                bool actual = processor.ValidateUser(firstName, lastName, userId);

                Assert.AreEqual(expected, actual);
            }

            [TestMethod]
            public void ValidateUser_GivenUseridEquals67986_ReturnsFalse()
            {
                var processor = new TransactionProcessor();
                string firstName = "Test";
                string lastName = "Test";
                long userId = 67986;
                bool expected = false;

                bool actual = processor.ValidateUser(firstName, lastName, userId);

                Assert.AreEqual(expected, actual);
            }

            [TestMethod]
            public void ValidateUser_GivenValidUserid_ReturnsFalse()
            {
                var processor = new TransactionProcessor();
                string firstName = "Test";
                string lastName = "Test";
                long userId = 1234;
                bool expected = true;

                bool actual = processor.ValidateUser(firstName, lastName, userId);

                Assert.AreEqual(expected, actual);
            }
        }

        [TestClass]
        public class ProcessTransactionTest
        {
            [TestMethod]
            public void ProcessTransaction_GivenNegativeUserid_ThrowsArgumentException()
            {
                var mockDateTimeHelper = new Mock<IDateTimeHelper>();
                mockDateTimeHelper
                    .Setup(mdth => mdth.CurrentTimeStamp())
                    .Returns(new DateTime(2018, 1, 1));
                    
                var processor = new TransactionProcessor(mockDateTimeHelper.Object);

                var userId = -1234;
                var transactionInstructions = "<test></test>";

                Assert.ThrowsException<ArgumentException>(() => processor.ProcessTransaction(userId, transactionInstructions));
            }

            [TestMethod]
            public void ProcessTransaction_GivenUseridEqualsZero_ThrowsArgumentException()
            {
                var mockDateTimeHelper = new Mock<IDateTimeHelper>();
                mockDateTimeHelper
                    .Setup(mdth => mdth.CurrentTimeStamp())
                    .Returns(new DateTime(2018, 1, 1));
                    
                var processor = new TransactionProcessor(mockDateTimeHelper.Object);

                var userId = 0;
                var transactionInstructions = "<test></test>";

                Assert.ThrowsException<ArgumentException>(() => processor.ProcessTransaction(userId, transactionInstructions));
            }

            [TestMethod]
            public void ProcessTransaction_TransactionInstructionsAreInvalidXml_ThrowsArgumentException()
            {
                var mockDateTimeHelper = new Mock<IDateTimeHelper>();
                mockDateTimeHelper
                    .Setup(mdth => mdth.CurrentTimeStamp())
                    .Returns(new DateTime(2018, 1, 1));
                    
                var processor = new TransactionProcessor(mockDateTimeHelper.Object);

                var userId = 1234;
                var transactionInstructions = "InvalidXml";

                Assert.ThrowsException<ArgumentException>(() => processor.ProcessTransaction(userId, transactionInstructions));
            }

            [TestMethod]
            public void ProcessTransaction_GivenValidInput_CallsRepositoryThatIDoNotFeelLikeMocking()
            {
                var mockDateTimeHelper = new Mock<IDateTimeHelper>();
                mockDateTimeHelper
                    .Setup(mdth => mdth.CurrentTimeStamp())
                    .Returns(new DateTime(2018, 1, 1));

                // Code would look like this, except instead of any string I would make sure that 
                // the string provided is in the correct format with the correct values passed in.
                /*
                var mockRepository = new Mock<IMockRepository>();
                mockRepository
                    .Setup(mr => mr.Insert(It.IsAny<String>()));
                */
                    
                var processor = new TransactionProcessor(mockDateTimeHelper.Object);

                var userId = 1234;
                var transactionInstructions = "<test></test>";

                processor.ProcessTransaction(userId, transactionInstructions);

                // Except it would be verifying the mock repository call, if I felt like making one
                mockDateTimeHelper.Verify(
                    mdth => mdth.CurrentTimeStamp(), Times.Once()
                );
            }
        }
    }
}